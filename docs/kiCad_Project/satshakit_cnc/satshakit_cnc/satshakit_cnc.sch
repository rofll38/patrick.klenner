EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 12447 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4100 4600 3900 4600
Text Label 3900 4600 2    70   ~ 0
VCC
Wire Wire Line
	4100 4700 3900 4700
Text Label 3900 4700 2    70   ~ 0
VCC
Wire Wire Line
	4100 4900 3900 4900
Text Label 3900 4900 2    70   ~ 0
VCC
Wire Wire Line
	4100 5000 3900 5000
Text Label 3900 5000 2    70   ~ 0
VCC
Wire Wire Line
	2800 3500 2500 3500
Text Label 2500 3500 2    70   ~ 0
VCC
Wire Wire Line
	900  3000 1200 3000
Wire Wire Line
	1200 3000 1700 3000
Wire Wire Line
	2200 3000 1700 3000
Wire Wire Line
	1700 2700 1700 3000
Wire Wire Line
	1200 2700 1200 3000
Wire Wire Line
	2200 2700 2200 3000
Connection ~ 1700 3000
Connection ~ 1200 3000
Text Label 900  3000 0    10   ~ 0
VCC
Wire Wire Line
	4100 4100 3300 4100
Wire Wire Line
	3300 4100 2600 4100
Wire Wire Line
	2600 4100 2600 3800
Connection ~ 3300 4100
Wire Wire Line
	4100 4300 3300 4300
Wire Wire Line
	3300 4300 2600 4300
Wire Wire Line
	2600 4300 2600 4600
Connection ~ 3300 4300
Wire Wire Line
	900  2100 1200 2100
Wire Wire Line
	1200 2100 1700 2100
Wire Wire Line
	1700 2100 2200 2100
Wire Wire Line
	1200 2400 1200 2100
Wire Wire Line
	1700 2400 1700 2100
Wire Wire Line
	2200 2400 2200 2100
Connection ~ 1200 2100
Connection ~ 1700 2100
Text Label 900  2100 0    10   ~ 0
GND
Wire Wire Line
	4100 5700 3900 5700
Text Label 3900 5700 2    70   ~ 0
GND
Wire Wire Line
	4100 5800 3900 5800
Text Label 3900 5800 2    70   ~ 0
GND
Wire Wire Line
	2300 3800 2100 3800
Text Label 2100 3800 2    70   ~ 0
GND
Wire Wire Line
	2300 4600 2100 4600
Text Label 2100 4600 2    70   ~ 0
GND
Wire Wire Line
	4100 5500 3900 5500
Text Label 3900 5500 2    70   ~ 0
GND
Wire Wire Line
	2100 6600 2000 6600
Text Label 2000 6600 2    70   ~ 0
GND
Wire Wire Line
	2600 6600 2500 6600
Wire Wire Line
	4100 3500 3950 3500
$Comp
L satshakit_cnc-eagle-import:ATMEGA48_88_168-AU MICRO1
U 1 1 B7F66DC5
P 5300 4600
F 0 "MICRO1" H 4300 5900 59  0000 L TNN
F 1 "ATMEGA328P-AU" H 4300 3200 59  0000 L BNN
F 2 "satshakit_cnc:TQFP32-08" H 5300 4600 50  0001 C CNN
F 3 "" H 5300 4600 50  0001 C CNN
	1    5300 4600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:VCC #P+07
U 1 1 FAFE20BD
P 800 3000
F 0 "#P+07" H 800 3000 50  0001 C CNN
F 1 "VCC" V 700 2900 59  0000 L BNN
F 2 "" H 800 3000 50  0001 C CNN
F 3 "" H 800 3000 50  0001 C CNN
	1    800  3000
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CSM-7X-DU CRYSTAL1
U 1 1 99621192
P 3300 4200
F 0 "CRYSTAL1" H 3400 4240 59  0000 L BNN
F 1 "16Mhz" H 3400 4100 59  0000 L BNN
F 2 "Fab:Crystal_NX5032GA-20" H 3300 4200 50  0001 C CNN
F 3 "" H 3300 4200 50  0001 C CNN
	1    3300 4200
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:0612ZC225MAT2A C1
U 1 1 7EE2FFE3
P 2300 3800
F 0 "C1" H 2264 3909 69  0000 L BNN
F 1 "22pF" H 2225 3492 69  0000 L BNN
F 2 "Fab:C_1206" H 2300 3800 50  0001 C CNN
F 3 "" H 2300 3800 50  0001 C CNN
	1    2300 3800
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:0612ZC225MAT2A C2
U 1 1 1A664D53
P 2300 4600
F 0 "C2" H 2264 4709 69  0000 L BNN
F 1 "22pF" H 2225 4292 69  0000 L BNN
F 2 "Fab:C_1206" H 2300 4600 50  0001 C CNN
F 3 "" H 2300 4600 50  0001 C CNN
	1    2300 4600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:GND #GND01
U 1 1 8DA2BB24
P 800 2100
F 0 "#GND01" H 800 2100 50  0001 C CNN
F 1 "GND" H 700 2000 59  0000 L BNN
F 2 "" H 800 2100 50  0001 C CNN
F 3 "" H 800 2100 50  0001 C CNN
	1    800  2100
	0    1    1    0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:RESISTOR1206 R1
U 1 1 5FC72806
P 3000 3500
F 0 "R1" H 2850 3559 59  0000 L BNN
F 1 "10k" H 2850 3370 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 3000 3500 50  0001 C CNN
F 3 "" H 3000 3500 50  0001 C CNN
	1    3000 3500
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:LED1206 LED_13
U 1 1 D663D747
P 2700 6600
F 0 "LED_13" V 2840 6520 59  0000 L BNN
F 1 "YELLOW" V 2925 6520 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 2700 6600 50  0001 C CNN
F 3 "" H 2700 6600 50  0001 C CNN
	1    2700 6600
	0    1    1    0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:RESISTOR1206 R3
U 1 1 4F7F9F8C
P 2300 6600
F 0 "R3" H 2150 6659 59  0000 L BNN
F 1 "499" H 2150 6470 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 2300 6600 50  0001 C CNN
F 3 "" H 2300 6600 50  0001 C CNN
	1    2300 6600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C3
U 1 1 C3EA450F
P 2200 2500
F 0 "C3" H 2260 2615 59  0000 L BNN
F 1 "10uF" H 2260 2415 59  0000 L BNN
F 2 "Fab:C_1206" H 2200 2500 50  0001 C CNN
F 3 "" H 2200 2500 50  0001 C CNN
	1    2200 2500
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C4
U 1 1 674C1B16
P 1700 2500
F 0 "C4" H 1760 2615 59  0000 L BNN
F 1 "1uF" H 1760 2415 59  0000 L BNN
F 2 "Fab:C_1206" H 1700 2500 50  0001 C CNN
F 3 "" H 1700 2500 50  0001 C CNN
	1    1700 2500
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C5
U 1 1 8E04CE4C
P 1200 2500
F 0 "C5" H 1260 2615 59  0000 L BNN
F 1 "100nF" H 1260 2415 59  0000 L BNN
F 2 "Fab:C_1206" H 1200 2500 50  0001 C CNN
F 3 "" H 1200 2500 50  0001 C CNN
	1    1200 2500
	-1   0    0    1   
$EndComp
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm JoystickPins1
U 1 1 6197B023
P 9000 3600
F 0 "JoystickPins1" H 8972 3532 50  0000 R CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 8972 3623 50  0000 R CNN
F 2 "fab:PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 9000 3600 50  0001 C CNN
F 3 "~" H 9000 3600 50  0001 C CNN
	1    9000 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 3500 8800 3500
Wire Wire Line
	6500 3600 8800 3600
Wire Wire Line
	8800 3700 6500 3700
Wire Wire Line
	7750 4250 7750 4000
Wire Wire Line
	7750 4000 6500 4000
Wire Wire Line
	7850 4150 7850 3900
Wire Wire Line
	7850 3900 6500 3900
Wire Wire Line
	7950 4050 7950 3800
Wire Wire Line
	7950 3800 6500 3800
$Comp
L fab:Conn_PinHeader_1x09_P2.54mm_Vertical_THT_D1.4mm LedStripesPins1
U 1 1 619936DA
P 9000 5200
F 0 "LedStripesPins1" H 8972 5132 50  0000 R CNN
F 1 "Conn_PinHeader_1x09_P2.54mm_Vertical_THT_D1.4mm" H 8972 5223 50  0000 R CNN
F 2 "fab:PinHeader_1x09_P2.54mm_Vertical_THT_D1.4mm" H 9000 5200 50  0001 C CNN
F 3 "~" H 9000 5200 50  0001 C CNN
	1    9000 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	8800 5600 8550 5600
Wire Wire Line
	8800 5500 6500 5500
Wire Wire Line
	8800 5400 6500 5400
Wire Wire Line
	8800 5300 6500 5300
Wire Wire Line
	8800 5200 6800 5200
Wire Wire Line
	6800 5200 6800 5100
Wire Wire Line
	6800 5100 6500 5100
Wire Wire Line
	8800 5100 6900 5100
Wire Wire Line
	6900 5100 6900 5000
Wire Wire Line
	6900 5000 6500 5000
Wire Wire Line
	8800 5000 7000 5000
Wire Wire Line
	7000 5000 7000 4900
Wire Wire Line
	7000 4900 6500 4900
Wire Wire Line
	8800 4900 7100 4900
Wire Wire Line
	7100 4900 7100 4800
Wire Wire Line
	7100 4800 6500 4800
Wire Wire Line
	8800 4800 7200 4800
Wire Wire Line
	7200 4800 7200 4700
Wire Wire Line
	7200 4700 6500 4700
Text GLabel 8550 3850 0    50   Input ~ 0
RESET
Text GLabel 3950 3400 1    50   Input ~ 0
ButtonPin5
Wire Wire Line
	3950 3400 3950 3500
Connection ~ 3950 3500
Wire Wire Line
	3950 3500 3200 3500
Wire Wire Line
	7250 4350 7250 4400
Wire Wire Line
	7250 4400 6500 4400
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm ProgrammingPins1
U 1 1 619B14DA
P 9000 5800
F 0 "ProgrammingPins1" H 8972 5732 50  0000 R CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 8972 5823 50  0000 R CNN
F 2 "fab:PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 9000 5800 50  0001 C CNN
F 3 "~" H 9000 5800 50  0001 C CNN
	1    9000 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	8550 5600 8550 5700
Wire Wire Line
	8550 5700 8800 5700
Connection ~ 8550 5600
Wire Wire Line
	8550 5600 6500 5600
Wire Wire Line
	8800 5800 7350 5800
Wire Wire Line
	7350 5800 7350 5700
Wire Wire Line
	7350 5700 6500 5700
Wire Wire Line
	8800 5900 7250 5900
Wire Wire Line
	7250 5900 7250 5800
Wire Wire Line
	7250 5800 6650 5800
Wire Wire Line
	6650 5800 6650 6600
Wire Wire Line
	6650 6600 2900 6600
Connection ~ 6650 5800
Wire Wire Line
	6650 5800 6500 5800
$Comp
L satshakit_cnc-eagle-import:M01PTH RST1
U 1 1 061EB7C7
P 2700 1500
F 0 "RST1" H 2600 1630 59  0000 L BNN
F 1 "M01PTH" H 2600 1300 59  0000 L BNN
F 2 "satshakit_cnc:1X01" H 2700 1500 50  0001 C CNN
F 3 "" H 2700 1500 50  0001 C CNN
	1    2700 1500
	0    1    1    0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C6
U 1 1 21A596BE
P 3700 2900
F 0 "C6" H 3760 3015 59  0000 L BNN
F 1 "100nF" H 3760 2815 59  0000 L BNN
F 2 "Fab:C_1206" H 3700 2900 50  0001 C CNN
F 3 "" H 3700 2900 50  0001 C CNN
	1    3700 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2700 2900 2700 1800
Wire Wire Line
	3200 2900 2700 2900
Connection ~ 3200 2900
Wire Wire Line
	3200 2900 3500 2900
Connection ~ 3200 3500
Wire Wire Line
	3200 3500 3200 2900
Wire Wire Line
	3200 2900 3100 2900
$Comp
L fab:Conn_PinHeader_1x06_P2.54mm_Vertical_THT_D1.4mm J1
U 1 1 619C8213
P 9000 4150
F 0 "J1" H 8972 4032 50  0000 R CNN
F 1 "Conn_PinHeader_1x06_P2.54mm_Vertical_THT_D1.4mm" H 8972 4123 50  0000 R CNN
F 2 "fab:PinHeader_1x06_P2.54mm_Vertical_THT_D1.4mm" H 9000 4150 50  0001 C CNN
F 3 "https://media.digikey.com/PDF/Data%20Sheets/Sullins%20PDFs/xRxCzzzSxxN-RC_ST_11635-B.pdf" H 9000 4150 50  0001 C CNN
	1    9000 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	8800 4350 7250 4350
Wire Wire Line
	8800 4050 7950 4050
Wire Wire Line
	8800 4150 7850 4150
Wire Wire Line
	8800 4250 7750 4250
Wire Wire Line
	8550 3850 8800 3850
Wire Wire Line
	3800 2900 4400 2900
Text GLabel 8550 3950 0    50   Input ~ 0
Interface
Wire Wire Line
	8550 3950 8800 3950
Text GLabel 4400 2900 2    50   Input ~ 0
InferfacePin
$EndSCHEMATC
