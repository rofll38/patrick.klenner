EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 12447 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FoDF_W07-rescue:ATMEGA48_88_168-AU-satshakit_cnc-eagle-import MICRO1
U 1 1 61951219
P 6250 4100
F 0 "MICRO1" H 6250 5486 59  0000 C CNN
F 1 "ATMEGA48_88_168-AU" H 6250 5381 59  0000 C CNN
F 2 "Fab:TQFP-32_7x7mm_P0.8mm" H 6250 4100 50  0001 C CNN
F 3 "" H 6250 4100 50  0001 C CNN
	1    6250 4100
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm Joystick1
U 1 1 61953DB4
P 9200 3200
F 0 "Joystick1" H 9172 3132 50  0000 R CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 9172 3223 50  0000 R CNN
F 2 "eagle:03P" H 9200 3200 50  0001 C CNN
F 3 "~" H 9200 3200 50  0001 C CNN
	1    9200 3200
	-1   0    0    1   
$EndComp
Wire Wire Line
	7450 3100 9000 3100
Wire Wire Line
	7450 3200 9000 3200
Wire Wire Line
	9000 3300 7450 3300
Text GLabel 8700 3600 0    50   Input ~ 0
RESET
Wire Wire Line
	8700 3600 8900 3600
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm MOSI_MISO_SCK1
U 1 1 6196C3F9
P 9200 5200
F 0 "MOSI_MISO_SCK1" H 9172 5132 50  0000 R CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 9172 5223 50  0000 R CNN
F 2 "eagle:03P" H 9200 5200 50  0001 C CNN
F 3 "~" H 9200 5200 50  0001 C CNN
	1    9200 5200
	-1   0    0    1   
$EndComp
Text GLabel 4500 2850 1    50   Input ~ 0
RESET
Wire Wire Line
	5050 3000 4500 3000
Wire Wire Line
	4500 2850 4500 3000
Connection ~ 4500 3000
Wire Wire Line
	4500 3000 4000 3000
$Comp
L fab:R R1
U 1 1 6197530F
P 3850 3000
F 0 "R1" V 3643 3000 50  0000 C CNN
F 1 "10k" V 3734 3000 50  0000 C CNN
F 2 "Fab:R_1206" V 3780 3000 50  0001 C CNN
F 3 "~" H 3850 3000 50  0001 C CNN
	1    3850 3000
	0    1    1    0   
$EndComp
$Comp
L fab:Power_+5V #PWR0101
U 1 1 619772E3
P 1050 7200
F 0 "#PWR0101" H 1050 7050 50  0001 C CNN
F 1 "Power_+5V" V 1065 7328 50  0000 L CNN
F 2 "" H 1050 7200 50  0001 C CNN
F 3 "" H 1050 7200 50  0001 C CNN
	1    1050 7200
	0    -1   -1   0   
$EndComp
$Comp
L fab:Power_GND #PWR0102
U 1 1 619783B9
P 1050 6700
F 0 "#PWR0102" H 1050 6450 50  0001 C CNN
F 1 "Power_GND" V 1055 6572 50  0000 R CNN
F 2 "" H 1050 6700 50  0001 C CNN
F 3 "" H 1050 6700 50  0001 C CNN
	1    1050 6700
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 6700 4800 6800
Wire Wire Line
	4800 7200 4450 7200
$Comp
L fab:C C3
U 1 1 61954289
P 4800 6950
F 0 "C3" H 4915 6996 50  0000 L CNN
F 1 "10uF" H 4915 6905 50  0000 L CNN
F 2 "Fab:C_1206" H 4838 6800 50  0001 C CNN
F 3 "" H 4800 6950 50  0001 C CNN
	1    4800 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 7100 4800 7200
$Comp
L fab:C C4
U 1 1 61954B14
P 4450 6950
F 0 "C4" H 4565 6996 50  0000 L CNN
F 1 "1uF" H 4565 6905 50  0000 L CNN
F 2 "Fab:C_1206" H 4488 6800 50  0001 C CNN
F 3 "" H 4450 6950 50  0001 C CNN
	1    4450 6950
	1    0    0    -1  
$EndComp
$Comp
L fab:C C5
U 1 1 619550A8
P 4100 6950
F 0 "C5" H 4215 6996 50  0000 L CNN
F 1 "100nF" H 4215 6905 50  0000 L CNN
F 2 "Fab:C_1206" H 4138 6800 50  0001 C CNN
F 3 "" H 4100 6950 50  0001 C CNN
	1    4100 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 6700 4450 6800
Connection ~ 4450 6700
Wire Wire Line
	4450 6700 4800 6700
Wire Wire Line
	4100 6800 4100 6700
Wire Wire Line
	4100 6700 4450 6700
Wire Wire Line
	4100 7100 4100 7200
Wire Wire Line
	4450 7100 4450 7200
Connection ~ 4450 7200
Wire Wire Line
	4450 7200 4100 7200
Wire Wire Line
	5050 3600 4000 3600
Wire Wire Line
	3500 3600 3500 3250
Wire Wire Line
	4500 4100 5050 4100
Wire Wire Line
	5050 4200 4500 4200
Wire Wire Line
	5050 4400 4500 4400
Wire Wire Line
	5050 4500 4500 4500
Wire Wire Line
	5050 5000 4500 5000
Wire Wire Line
	4500 5200 5050 5200
Text Label 2750 3250 2    50   ~ 0
GND
Text Label 2750 4150 2    50   ~ 0
GND
Wire Wire Line
	7550 5900 5400 5900
$Comp
L fab:LED LED_13
U 1 1 6198BC82
P 5250 5900
F 0 "LED_13" H 5243 6116 50  0000 C CNN
F 1 "YELLOW" H 5243 6025 50  0000 C CNN
F 2 "Fab:LED_1206" H 5250 5900 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 5250 5900 50  0001 C CNN
	1    5250 5900
	1    0    0    -1  
$EndComp
$Comp
L fab:R R3
U 1 1 6198CE90
P 4650 5900
F 0 "R3" V 4443 5900 50  0000 C CNN
F 1 "499" V 4534 5900 50  0000 C CNN
F 2 "Fab:R_1206" V 4580 5900 50  0001 C CNN
F 3 "~" H 4650 5900 50  0001 C CNN
	1    4650 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 5900 4800 5900
Wire Wire Line
	4500 5900 4100 5900
Wire Wire Line
	5050 5300 4500 5300
Text Label 4100 5900 2    50   ~ 0
GND
$Comp
L fab:C C1
U 1 1 619976B8
P 3350 3250
F 0 "C1" V 3098 3250 50  0000 C CNN
F 1 "22pF" V 3189 3250 50  0000 C CNN
F 2 "Fab:C_1206" H 3388 3100 50  0001 C CNN
F 3 "" H 3350 3250 50  0001 C CNN
	1    3350 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 3250 2750 3250
$Comp
L fab:C C2
U 1 1 61998437
P 3350 4150
F 0 "C2" V 3098 4150 50  0000 C CNN
F 1 "22pF" V 3189 4150 50  0000 C CNN
F 2 "Fab:C_1206" H 3388 4000 50  0001 C CNN
F 3 "" H 3350 4150 50  0001 C CNN
	1    3350 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 4150 2750 4150
Wire Wire Line
	2000 7050 2000 7200
Wire Wire Line
	2000 6700 2000 6850
Wire Wire Line
	7450 3900 9000 3900
Wire Wire Line
	7450 5100 9000 5100
Wire Wire Line
	7450 5200 9000 5200
Wire Wire Line
	7450 5300 7550 5300
Wire Wire Line
	7550 5900 7550 5300
Connection ~ 7550 5300
Wire Wire Line
	7550 5300 9000 5300
Text GLabel 1500 6950 0    50   Input ~ 0
LED_Data
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm Movementbuttons1
U 1 1 61BF6CBD
P 9200 4500
F 0 "Movementbuttons1" H 9172 4432 50  0000 R CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 9172 4523 50  0000 R CNN
F 2 "eagle:03P" H 9200 4500 50  0001 C CNN
F 3 "~" H 9200 4500 50  0001 C CNN
	1    9200 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	7450 4400 9000 4400
Wire Wire Line
	7450 4500 9000 4500
Wire Wire Line
	7450 4600 9000 4600
Wire Wire Line
	7450 3400 9000 3400
Wire Wire Line
	7450 3500 9000 3500
$Comp
L fab:Crystal_NX5032GA-20 Y1
U 1 1 61B0D0FF
P 4000 3750
F 0 "Y1" V 4046 3619 50  0000 R CNN
F 1 "Crystal_NX5032GA-20" V 3955 3619 50  0000 R CNN
F 2 "crystal:01_FAB_HELLO_CSM-7X-DU" H 4000 3750 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/NDK%20PDFs/NX5032GA.pdf" H 4000 3750 50  0001 C CNN
	1    4000 3750
	0    -1   -1   0   
$EndComp
Connection ~ 4000 3600
Wire Wire Line
	4000 3600 3500 3600
Wire Wire Line
	5050 3800 4550 3800
Wire Wire Line
	4550 3800 4550 3900
Wire Wire Line
	4550 3900 4000 3900
Wire Wire Line
	3500 3900 3500 4150
Connection ~ 4000 3900
Wire Wire Line
	4000 3900 3500 3900
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm LEDs(M)1
U 1 1 61B22162
P 1800 6950
F 0 "LEDs(M)1" V 1908 7231 50  0000 C CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" V 1908 7140 50  0000 C CNN
F 2 "eagle:03P" H 1800 6950 50  0001 C CNN
F 3 "~" H 1800 6950 50  0001 C CNN
	1    1800 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 6950 1500 6950
Wire Wire Line
	1050 6700 2000 6700
Wire Wire Line
	1050 7200 2000 7200
Connection ~ 4100 6700
Wire Wire Line
	3500 6700 4100 6700
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm Vcc_Chip_Joystick_Display1
U 1 1 61B9643C
P 3400 7000
F 0 "Vcc_Chip_Joystick_Display1" V 3462 7144 50  0000 L CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" V 3553 7144 50  0000 L CNN
F 2 "eagle:03P" H 3400 7000 50  0001 C CNN
F 3 "~" H 3400 7000 50  0001 C CNN
	1    3400 7000
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 6700 3400 6700
Connection ~ 3500 6700
Connection ~ 3400 6700
Wire Wire Line
	3400 6700 3500 6700
Wire Wire Line
	9000 4000 7450 4000
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm GND_Chip_Joystick_Display1
U 1 1 61B8B4B4
P 3400 6500
F 0 "GND_Chip_Joystick_Display1" V 3462 6644 50  0000 L CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" V 3553 6644 50  0000 L CNN
F 2 "eagle:03P" H 3400 6500 50  0001 C CNN
F 3 "~" H 3400 6500 50  0001 C CNN
	1    3400 6500
	0    1    1    0   
$EndComp
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm RXD_TXD_Din1
U 1 1 61BA4E5B
P 9200 4000
F 0 "RXD_TXD_Din1" H 9172 3932 50  0000 R CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 9172 4023 50  0000 R CNN
F 2 "eagle:03P" H 9200 4000 50  0001 C CNN
F 3 "~" H 9200 4000 50  0001 C CNN
	1    9200 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	7450 4100 9000 4100
Wire Wire Line
	4100 7200 3500 7200
Connection ~ 4100 7200
Wire Wire Line
	3400 7200 3500 7200
Connection ~ 3500 7200
Wire Wire Line
	3400 7200 3300 7200
Connection ~ 3400 7200
Wire Wire Line
	3300 7200 2000 7200
Connection ~ 3300 7200
Connection ~ 2000 7200
$Comp
L fab:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm GND_Buttons1
U 1 1 61BFCA26
P 2800 6500
F 0 "GND_Buttons1" V 2862 6644 50  0000 L CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" V 2953 6644 50  0000 L CNN
F 2 "eagle:03P" H 2800 6500 50  0001 C CNN
F 3 "~" H 2800 6500 50  0001 C CNN
	1    2800 6500
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 6700 2900 6700
Connection ~ 3300 6700
Wire Wire Line
	2900 6700 2800 6700
Connection ~ 2900 6700
Wire Wire Line
	2700 6700 2800 6700
Connection ~ 2800 6700
Wire Wire Line
	2700 6700 2000 6700
Connection ~ 2700 6700
Connection ~ 2000 6700
Text GLabel 8000 3000 2    50   Input ~ 0
LED_Data
Wire Wire Line
	4800 6700 5900 6700
Connection ~ 4800 6700
Wire Wire Line
	4800 7200 5900 7200
Connection ~ 4800 7200
Text GLabel 4500 5000 0    50   Input ~ 0
GND
Text GLabel 4500 4100 0    50   Input ~ 0
Vcc
Text GLabel 4500 4200 0    50   Input ~ 0
Vcc
Text GLabel 4500 4400 0    50   Input ~ 0
Vcc
Text GLabel 4500 4500 0    50   Input ~ 0
Vcc
Text GLabel 4500 5200 0    50   Input ~ 0
GND
Text GLabel 4500 5300 0    50   Input ~ 0
GND
Wire Wire Line
	3700 3000 3500 3000
Text GLabel 5900 6700 2    50   Input ~ 0
GND
Text GLabel 5900 7200 2    50   Input ~ 0
Vcc
Wire Wire Line
	8000 3000 7450 3000
Text GLabel 3500 3000 0    50   Input ~ 0
Vcc
Wire Wire Line
	8900 3600 8900 3700
Wire Wire Line
	8900 3700 9000 3700
Connection ~ 8900 3600
Wire Wire Line
	8900 3600 9000 3600
$Comp
L fab:Conn_PinHeader_1x04_P2.54mm_Vertical_THT_D1.4mm SDA_SCL_RESET_RESET1
U 1 1 61CE6680
P 9200 3600
F 0 "SDA_SCL_RESET_RESET1" H 9172 3482 50  0000 R CNN
F 1 "Conn_PinHeader_1x04_P2.54mm_Vertical_THT_D1.4mm" H 9172 3573 50  0000 R CNN
F 2 "eagle:04P" H 9200 3600 50  0001 C CNN
F 3 "~" H 9200 3600 50  0001 C CNN
	1    9200 3600
	-1   0    0    1   
$EndComp
$EndSCHEMATC
