/*tutorial: 
 * https://www.electroniclinic.com/arduino-ws2812b-led-strip-connection-and-code/
*/
#include <FastLED.h>
#define LED_PIN A3
#define NUM_LEDS 144

CRGB leds[NUM_LEDS];  // set an array for the used LEDs
void setup() {
  // put your setup code here, to run once:
  FastLED.addLeds<WS2812, LED_PIN, RGB>(leds, NUM_LEDS);  // set the manufacturing type of LED stripes, the LED pin and the type of the LED
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 2000);        // set the maximum Volts and Ampere

  FastLED.clear();    //clear out the data
 
  FastLED.show();     //send the data to the LEDs
}

void loop() {
  // put your main code here, to run repeatedly:
  // RED Green Blue
  
  //the LEDs needs to be activated first
  for (int i=0; i<NUM_LEDS; i++ )
  {
      leds[i] = CRGB(255, 255, 255 );   //configures the color of the LEDs
      FastLED.setBrightness(255);       //configures the brightness
      FastLED.show();
  
  } 

  // let all 81 LEDs blink
  for (int i=0; i<NUM_LEDS; i++ )
  {
      leds[i] = CRGB(255, 255, 255 );
      FastLED.setBrightness(255);
      FastLED.show();
      delay (1000);
      FastLED.setBrightness(0);
      FastLED.show();
      delay (1000);
  } 

}
