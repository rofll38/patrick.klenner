#include <SoftwareSerial.h>

#define LED_PIN 5
#define BUTTON_PIN 6
#define ITERATIONS 2

SoftwareSerial mySerial(3,4); //RX;TX
boolean buttonState;
byte counter = 0;
int sensorValue = 0;

void setup() {
  // put your setup code here, to run once:

  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);
  mySerial.begin(4800); // BAUD rate should not be more than 4800

}

void loop() {
  // put your main code here, to run repeatedly:
  buttonState = digitalRead(BUTTON_PIN);

  // the digital input pins of the ATmega328 are set to HIGH by default
  if( buttonState){
    // if the counter is lower than the presetted constant ITERATIONS, then execute the code inside the curly brackets
      if( counter < ITERATIONS)
      {
            morseSOS(); // calls the "morseSOS" function
            mySerial.print("Signal Iteration no. "); //prints the string (the character chain) into the serial monitor
            mySerial.println(counter + 1);
            counter++; // increases the varaible by 1
            delay(1000); // stop the code for 1 second
      }
    }
    else{
      counter = 0; // reset the counter
      serialCommunication(); // call the "serialCommunication" function
    }

  
  

  
}

// sends the message to the serial ouput
void serialCommunication(){
  mySerial.println("SOS signal restarted");
  delay(1000);
}

// set the LED to HIGH or LOW acording to the morse code for "SOS" (3 long, 3 short, 3 long)
void morseSOS(){
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);

  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(100);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(100);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(100);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(100);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(100);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);

  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);



  
}
