#include <SoftwareSerial.h>


SoftwareSerial mySerial(7,8); //RX;TX

void setup() {
  // put your setup code here, to run once:
  mySerial.begin(4800); // BAUD rate should not be more than 4800
}

void loop() {
  // put your main code here, to run repeatedly:
  mySerial.println("Hello!");
  delay(1000);
}
