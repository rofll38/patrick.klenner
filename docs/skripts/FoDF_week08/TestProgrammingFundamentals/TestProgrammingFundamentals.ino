#define LED_PIN 13
#define ITERATIONS 5
#define BUTTON_PIN 8
// test for FoDF
// define is for constants
// pullup is always preset on HIGH, 

byte counter = 0;
boolean buttonState;
void setup() {
  // put your setup code here, to run once:
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT); //this sets ledPin as an output pin
  Serial.begin(9600);
  
}

void loop() {
  buttonState = digitalRead(BUTTON_PIN);
  
  // put your main code here, to run repeatedly:

  /*
  if( buttonState){
      digitalWrite(LED_PIN, HIGH); 
    }
    else{
      digitalWrite(LED_PIN, LOW); 
    }
  */
    if( buttonState){
      if( counter < ITERATIONS)
      {
            blinking();
            Serial.print("Blink no. ");
            Serial.println(counter + 1);
            counter++;
            
      }
    }
    else{
      counter = 0;
    }
    
  
}

void blinking(){
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(100);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(100);
}
