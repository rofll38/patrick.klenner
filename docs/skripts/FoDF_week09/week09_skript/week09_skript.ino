#include <SoftwareSerial.h>

#define ANALOG_X A1
#define ANALOG_Y A2
#define BUTTON_PIN A3

SoftwareSerial mySerial(3,4); //RX;TX
int buttonState;

void setup() {
  // put your setup code here, to run once:

  pinMode(ANALOG_X, INPUT);
  pinMode(ANALOG_Y, INPUT);
  pinMode(BUTTON_PIN, INPUT);
  digitalWrite(BUTTON_PIN, HIGH);
  mySerial.begin(4800); // BAUD rate should not be more than 4800
  //Serial.begin(4800);
}

void loop() {
  // put your main code here, to run repeatedly:
  buttonState = digitalRead(BUTTON_PIN);
 
   // if the button of the joystick is pressed, print a note in the serail mointor and pause // the input reading for 2 seconds
   if(!buttonState){
    mySerial.println("#-----------------------------------#");
    mySerial.println("Button was pressed");
    mySerial.println("#-----------------------------------#\n");

    delay(2000);
  }

  // readout the values of the analoge pins
  int xValue = analogRead(ANALOG_X);
  int yValue = analogRead(ANALOG_Y);

  // write the x and y values into the serial monitor
  mySerial.print("x Value: ");
  mySerial.print(xValue);
  mySerial.print("  y Value: ");
  mySerial.println(yValue);

}
