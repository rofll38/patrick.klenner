/*tutorial: 
 * https://www.electroniclinic.com/arduino-ws2812b-led-strip-connection-and-code/
*/
#include <FastLED.h>
#define LED_PIN A0
#define NUM_LEDS 144

CRGB leds[NUM_LEDS];
void setup() {
  // put your setup code here, to run once:
  FastLED.addLeds<WS2812, LED_PIN, RGB>(leds, NUM_LEDS);
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 2000);

  FastLED.clear();
 
  FastLED.show();
}

void loop() {
  // put your main code here, to run repeatedly:
// RED Green Blue
for (int i=0; i<NUM_LEDS; i++ )
{
    leds[i] = CRGB(255, 255, 255 );
    FastLED.setBrightness(255);
    FastLED.show();
    delay (50);
} 

}
