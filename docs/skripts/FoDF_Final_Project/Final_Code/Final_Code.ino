/*tutorial: 
 * https://www.electroniclinic.com/arduino-ws2812b-led-strip-connection-and-code/
*/
#include <FastLED.h>



#define LED_PIN A0
#define NUM_LEDS 48

#define ANALOG_JOYSTICK_X A1
#define ANALOG_JOYSTICK_Y A2
#define DIGITAL_JOYSTICK_BUTTON A3

#define DIGITAL_10FT_BUTTON 5
#define DIGITAL_20FT_BUTTON 6
#define DIGITAL_30FT_BUTTON 7

int xPos;
int yPos;
int currentPosition = 0;
int selectPosition = 0;
int buttonState;
int mov10FtState;
int mov20FtState;
int mov30FtState;


CRGB leds[NUM_LEDS];  // set an array for the used LEDs




void setup() {
  // put your setup code here, to run once:
  FastLED.addLeds<WS2812, LED_PIN, RGB>(leds, NUM_LEDS);  // set the manufacturing type of LED stripes, the LED pin and the type of the LED
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 2000);        // set the maximum Volts and Ampere

  FastLED.clear();    //clear out the data
  FastLED.show();     //send the data to the LEDs


  //the LEDs needs to be activated first
  for (int i=0; i<NUM_LEDS; i++ )
  {
      leds[i] = CRGB(255, 255, 255 );   //configures the color of the LEDs
      FastLED.setBrightness(255);       //configures the brightness
      FastLED.show();
      leds[i] = CRGB(255, 255, 255 );   //configures the color of the LEDs
      FastLED.setBrightness(0);       //configures the brightness
      FastLED.show();
  
  }

  pinMode(ANALOG_JOYSTICK_X, INPUT);
  pinMode(ANALOG_JOYSTICK_Y, INPUT);
  pinMode(DIGITAL_JOYSTICK_BUTTON, INPUT);

  digitalWrite(DIGITAL_JOYSTICK_BUTTON, HIGH);

  

  pinMode(DIGITAL_10FT_BUTTON, INPUT);
  pinMode(DIGITAL_20FT_BUTTON, INPUT);
  pinMode(DIGITAL_30FT_BUTTON, INPUT);
  
  digitalWrite(DIGITAL_10FT_BUTTON, HIGH);
  digitalWrite(DIGITAL_20FT_BUTTON, HIGH);
  digitalWrite(DIGITAL_30FT_BUTTON, HIGH);
  
  
  Serial.begin(9600); // BAUD rate should not be more than 4800

}







void loop() {
  // put your main code here, to run repeatedly:
  // RED Green Blue
  
   buttonState = digitalRead(DIGITAL_JOYSTICK_BUTTON);
   mov10FtState = digitalRead(DIGITAL_10FT_BUTTON);
   mov20FtState = digitalRead(DIGITAL_20FT_BUTTON);
   mov30FtState = digitalRead(DIGITAL_30FT_BUTTON);


  // readout the values of the analoge pins
  int xValue = analogRead(ANALOG_JOYSTICK_X);
  int yValue = analogRead(ANALOG_JOYSTICK_Y);

  //when the button in the joystick is pressed: it selects the currentposition to claculate the movement range later on
  if(!buttonState){
    selectPosition = currentPosition;
    Serial.println("#-----------------------------------#");
    Serial.print("Selected Position: ");
    Serial.println(selectPosition);
    Serial.println("#-----------------------------------#\n");
    delay(1000);
  }


  
//--------------------------------------------------------------------------------

// conntrols for the current Position

 //border 
  if( xValue > 900){
      currentPosition = currentPosition + 1;
      if(currentPosition > NUM_LEDS - 1){
        currentPosition = NUM_LEDS - 1;
      }
      delay(100);
  }

  if( xValue < 200){
    currentPosition = currentPosition - 1;
     if(currentPosition < 0){
        currentPosition = 0;
     }
     delay(100);
  }
  
  /*
  if( yValue > 900){
     if( (currentPosition%2) == 0){
          currentPosition = currentPosition + 23;
          if(currentPosition > 143){
             currentPosition = 143;
          }
     }
     else{
          currentPosition = currentPosition + (12 - (currentPosition%12));
          if(currentPosition > 143){
            currentPosition = 143;
          }
     }
  }


  if( yValue < 200){
     if( (currentPosition%2) == 0){
            currentPosition = currentPosition - 23;
            if(currentPosition < 0){
              currentPosition = 0;
            }
     }
     else{
          currentPosition = currentPosition - (12 - (currentPosition%12));
          if(currentPosition < 0){
            currentPosition = 0;
          }
     }
  }
  */
//-----------------------------------------------------------------------  

//Serial communiction to check if the Values are properly read
  Serial.print(xValue);
  Serial.print("|");
  Serial.print(yValue);
  Serial.print("|");
  Serial.println(currentPosition);




  
//-----------------------------------------------------------------------


  //light up the LEDs
  int iteration = 0;
  while(iteration < NUM_LEDS){
      //show only the current position
      if( iteration == currentPosition){
        leds[currentPosition] = CRGB(255, 255, 255 );   //configures the color of the LEDs
        FastLED.setBrightness(255);       //configures the brightness
        FastLED.show();
      }
      else{
        leds[iteration] = CRGB(0, 0, 0 );   //configures the color of the LEDs
        FastLED.setBrightness(255);       //configures the brightness
        FastLED.show();
      }
    iteration++;
  }

  // every 10ft corresponds to 1 LED position
  if(!mov10FtState){
    showMovementRange(currentPosition, 1);
  }
  if(!mov20FtState){
    showMovementRange(currentPosition, 2);
  }
  if(!mov30FtState){
    showMovementRange(currentPosition
    , 3);
  }

  

}








//method to show the entered maximum movementrange from the inserted position
void showMovementRange(int currentPosition, int range){     
  
  int rangePositions[4];

  rangePositions[0] = currentPosition - range;
  rangePositions[1] = currentPosition + range;
  //rangePositions[2] = currentPosition;
  //rangePositions[3] = currentPosition;
  rangePositions[2] = (23 * range - (currentPosition%12)) - (currentPosition - (currentPosition%12)) ;
  rangePositions[3] = currentPosition + ((currentPosition%12) + 23 * range) - (currentPosition - (currentPosition%12));

  selectPosition = currentPosition;
  Serial.println("#-----------------------------------#");
  Serial.print("rangePositions[0]: ");
  Serial.println(rangePositions[0]);
  Serial.print("rangePositions[1]: ");
  Serial.println(rangePositions[1]);
  Serial.print("rangePositions[2]: ");
  Serial.println(rangePositions[2]);
  Serial.print("rangePositions[3]: ");
  Serial.println(rangePositions[3]);
  Serial.println("#-----------------------------------#\n");
  delay(1000);
  
  
  //light up the LEDs
  int iteration = 0;
  while(iteration < NUM_LEDS){

       //show the max range for 1 second      
      for(int i = 0; i < sizeof(rangePositions); i ++){
        if( iteration == rangePositions[i]){
          leds[rangePositions[i]] = CRGB(255, 255, 255 );   //configures the color of the LEDs
          FastLED.setBrightness(255);       //configures the brightness
          FastLED.show();
          if( currentPosition > 0 ){
            leds[0] = CRGB(0, 0, 0);   //configures the color of the LEDs
            FastLED.setBrightness(255);       //configures the brightness
            FastLED.show();
          }
        }

      }
      
      iteration++;
    }
         
    delay(1000);
    
    for(int i = 0; i < NUM_LEDS; i ++){
        leds[i] = CRGB(0, 0, 0 );   //configures the color of the LEDs
        FastLED.setBrightness(255);       //configures the brightness
        FastLED.show();
    }
 }

  
  
                 
  
