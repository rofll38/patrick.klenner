# 6. 3D Printing

This week I worked on 3d printing and getting to know of the limitations of this process.

## Sidenotes
Here are some dimensions and rules of thumb for the 3D printing:

- max Layerheight = nozile width * 0.7
  - here: 0.2 mm

Colors in Ultimaker Cura 4.11.0:
- orange = infill
- green = shell
- blue = build plate adhesion

General stuff:
- consider 0.4mm for formfit
- verbatim filament
- filament: 2.85mm thick

- 100g filament is free (just a sidenote for me, it's the filament from the Fablab resources)




## Useful links

- [Ultimaker Cura](https://ultimaker.com/software/ultimaker-cura)
- [Thingiverse](https://www.thingiverse.com/)
- [traceparts](https://www.traceparts.com/de)
- [grabcad](https://grabcad.com/)



## 3D Modelling
 
 Before you can print something, you need a model first. I modelled my figure in "Autodes Fusion360" but you can use every 3D modelling software you want (like "Blender"). But you should check if the software can export your file as a ``` .stl``` it is VERY IMPORTANT later on (for more information about that, scroll a bit down to the topic "3D printing").

I build my model form bottom to the top. Therefore I started with a podest and proceeded with the feets. To cut out the shapes, I created a sketch which I later on extruded. For the roundings I used the "fillet" function of Fusion360 and set it mostly to a value between 1mm and 2mm.

![Image01](../images/week06/3dModelling001.jpg)
![Image02](../images/week06/3dModelling002.jpg)
![Image03](../images/week06/3dModelling003.jpg)

<br>

After the feets I created the lower leg and the knees. I cut the upper parts of the knees flat since the model need a connection for the upper body.

![Image04](../images/week06/3dModelling004.jpg)
![Image05](../images/week06/3dModelling005.jpg)
![Image06](../images/week06/3dModelling006.jpg)
![Image07](../images/week06/3dModelling007.jpg)
![Image08](../images/week06/3dModelling008.jpg)

<br>

I wanted most of the body parts as individual Fuiso360 "Bodies" so I can move and edit them freely. But you set the reference area for your sketches on the existing parts. When you extrude the sketches just set the other parts to "invisible" for generating them as a individual part.

![Image09](../images/week06/3dModelling009.jpg)
![Image10](../images/week06/3dModelling010.jpg)

<br>

On top of the neck should be a head right? 

![Image11](../images/week06/3dModelling011.jpg)
![Image12](../images/week06/3dModelling012.jpg)
![Image13](../images/week06/3dModelling013.jpg)
![Image14](../images/week06/3dModelling014.jpg)
![Image15](../images/week06/3dModelling015.jpg)

I misjudged the size for the head. So I changed the size of the head with the "scale" function of Fusion360 until the proportions were satisfying.

![Image16](../images/week06/3dModelling016.jpg)

<br>

I think it is obvious now that this should be a skeleton. But a skeleton also needs arms:

![Image17](../images/week06/3dModelling017.jpg)
![Image18](../images/week06/3dModelling018.jpg)
![Image19](../images/week06/3dModelling019.jpg)
![Image20](../images/week06/3dModelling020.jpg)

<br>

The skeleton was a warrior and every warrior needs equipment right?
So I decided to give it a gladius and a round shield.

![Image21](../images/week06/inspiration.jpg)
![Image22](../images/week06/3dModelling021.jpg)
![Image23](../images/week06/3dModelling022.jpg)
![Image24](../images/week06/3dModelling023.jpg)

Now the model is ready to be printed! Just save it as a ```.stl``` file and you can proceed to the 3D printing part.
![Image25](../images/week06/3dModelling024.jpg)

<br>
<br>


## 3d Printing

Unlike milling, 3D printing is an additve manufacturing process. That means that instead of removing material, 3d printing is adding fine layers of filament on top of each other until the desired model is build. The filament is the material of which the product should be made of. In the most cases the filament are some kind of plastics like PLA but it can also be cement or even marzipan. In cas eof plastics, the filament is heated by the nozzles to a usable viscosity. 
Most of the 3D printers requiers some special file format. The type of software for that is called "slicer" since it calculates all the layers of the 3D model. In my case I used "Ultimaker Cura" since I used the "Ultimaker S5" 3D printer.
The slicer needs ``` .stl ``` file which you can export from your 3D modell programm. After you opend, configured and sliced everything for your 3D printer, you can save the "g-code" and put it in your 3D printer. The "g-code" contains the instructions for the 3D printer like the movement speed, filament feed, positions etc.

3D printing also does not have the dimension limitaions of CNC milling. But if your model has some overhanging parts (rule of thumb: >45°) than you should put some supports on those parts. A different kind of support is the "build plate adhesion" which ensures that the printed model does not move while the nozlle of the printer moves.

The overall process of 3D printing is quiet time intensive, so you can also change the "infill" and the "shell" to reduce time. The infill defines a strucual pattern like a hexagonal pattern so the inside of the model is partially hollow. The shell defines the outerparts of the inside. It ensures that the walls has still a strucutual strength so the modell does not collaps.


In the following you will see my configurations in Ultimaker Cura.

In the "prepare" function you can see all the parts which should have a support:

![Image26](../images/week06/slicer004.jpg)

In the bar below the "prepare" mode you can setup your printer model, the type of filament and how fast the process should be. On the right hand side of the image you can see some additional settings:
![Image27](../images/week06/slicer001.jpg)

Note that you also can do more advanced settings if you hover a bit left of the arrow selection (marked in green):

![Image28](../images/week06/slicer002.jpg)

After you clicked on "slice" you can click on"preview wher you can see all the path, layers and supports. Ultimaker Cura also shows you how long it takes and how much filament will be used.

![Image29](../images/week06/slicer003.jpg)




<br>
<br>


The 3D printer in action:

![Image30](../images/week06/3dprinting001.jpg)

And the final result:

![Image31](../images/week06/3dprinting002.jpg)


## Download

 - Fusion360 file: <a href="../layoutfiles/Figures_3d/Skeleton_v6.f3d" download>Skeleton_v6.f3d</a>
 - .stl file: <a href="../layoutfiles/Figures_3d/Skeleton_v6.stl" download>Skeleton_v6.stl</a>
 - Ultimaker Format Package: <a href="../layoutfiles/Figures_3d/UMS5_Skeleton_v6.ufp" download>UMS5_Skeleton_v6.ufp</a>
 - G-Code file: <a href="../layoutfiles/Figures_3d/UMS5_Skeleton_v6.gcode" download>UMS5_Skeleton_v6.gcode</a>
