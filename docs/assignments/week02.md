# 2. 2D and 3D Modelling

This week I learned a lot about two modelling softwares. These are "LibreCAD" for the 2D modelled parts and "Autodesk Fusion360" for the 3D modelled parts.


## Useful links
For LibreCAD:
- [LibreCAD user manual](https://librecad.readthedocs.io/en/latest/index.html)
- [LibreCAD tutorials](https://wiki.librecad.org/index.php?title=Tutorials)

<br>

For Fusion360:
- [Fusion 360 example, simulation and others](https://www.markdowntutorial.com/)
- [ Fusion 360 documentation](https://help.autodesk.com/view/fusion360/ENU/?guid=GUID-1C665B4D-7BF7-4FDF-98B0-AA7EE12B5AC2)
- [Fusion 360 courses](https://help.autodesk.com/view/fusion360/ENU/courses/)

## Sketch
The plan is to laser cut a borad with the dimensions 300mmx300mm with the playground of 260mmx260mm. The playground is divided into 81 20mmx20mm squares with each a cirle in the middle with a diameter of 5mm.
The whole board should be made out of wood and the circles out of plexiglas.

I chose these dimensions since the lasercutter has the process dimensions of 600mmx300mm. The tolerances of the laser itself are 0.05 – 0.4 mm.

![Image1](../images/week02/week02Sketch.jpg)

<br>
<br>
<br>

## Why using two seperate softwares?
First of all "LibreCAD" is an open source software and therefore you can get it for free. Second of all in this project I work with different kinds of machinery which is necessary to work with different kinds of material. For example there is no method (as of 11. October 2021) to 3D print with wood.

"Fusion360" from Autodesk for the 3D modelling is on the otherside not free.
LibreCAD files can be processed by the laser cutter while I am not sure about the files from Fusion360. So better save than sorry.

<br>
<br>
<br>

## 2D modelling the board
Every model witht the same material should be in the same file so I modelled the board first and then the plexiglas circles.

First I made the border with the outer dimensions of 300mmx300mm and the inner dimensions of 260mmx260mm:

![Image01](../images/week02/2dParts/step01.jpg)

I used the two point lines like in the following image:

![Image02](../images/week02/2dParts/step01_method.jpg)

<br>

With the same two point lines I made a grid for every position. I also created an additonal layer for the grid/playground.
![Image03](../images/week02/2dParts/step02.jpg)

<br>


I modelled one circle with the "Center, Point" function and set the radius to 5mm for one row and then I mirrored it. The circles are in a own layer too.
![Image04](../images/week02/2dParts/step03.jpg)
![Image05](../images/week02/2dParts/step03_method01.jpg)
![Image06](../images/week02/2dParts/step03_method02.jpg)
![Image07](../images/week02/2dParts/step03_method03.jpg)
![Image08](../images/week02/2dParts/step03_method04.jpg)
![Image09](../images/week02/2dParts/step03_method05.jpg)


<br>
Here are all the layers which I used:

![Image10](../images/week02/2dParts/step03_method03_sidenote.jpg)

For the plexiglas circles I used the same methode as before but I made them in a new file (since they are a different material):

![Image11](../images/week02/2dParts/plexiglascircles.jpg)

<br>

[Download 2D wood parts](../layoutfiles/Board_2d/Board_Layout_Wood.dxf~)
<br>

[Download 2D acrilics parts](../layoutfiles/Board_2d/MiddleCriclesPlexi.dxf~)

<br>
<br>
<br>

## 3D modelling the player pieces

So I will make an board game but for a board game I need some figures for the players. These are goning to be 3d printed and as I said before I used Fusion360 to modell those.
![Image12](../images/week02/3dParts/tokenDimensions.jpg)


First I started the sketch function for 2D desgins by clicking on "create" and select "Create Sketch".
For every token I created a new sketch so I could dis- and enable them in the view selection. I started every token by creating a circle with a 20mm diameter. By using "Line" I made all the straight lines and with "Fit Point Spline" I created the curves.

![Image13](../images/week02/3dParts/sketch01.jpg)

As you can see in the following image, I split the circles for the eyes of the skull in two by making a line through the circles. Later on these are going to be made into a half-ball with the "Revolve" function and for that I needed the middle line to revolve around.
To create the missing teeth for example, I needed to copy the sketches and move them 5mm above.

![Image14](../images/week02/3dParts/sketch02.jpg)

![Image15](../images/week02/3dParts/body01.jpg)

Here you can see all the final sketches:

![Image16](../images/week02/3dParts/sketch03.jpg)

And their extruded form:

![Image17](../images/week02/3dParts/body02.jpg)

The player tokens just loks like some kind of stamps, so lets give them some personal touch. I used the "Extrude" funciton to cut out a custom form out of the figures.

I needed first a reference area. A box fits this job well, since you can jsut pick your desired face and use it as a reference. For that I sketched a square with "center Rectangle" so the center of the square would be set to the center of the circle.

![Image18](../images/week02/3dParts/referenceBody01.jpg)

After that I extrude that square. To set one side of the square to a reference for the custom sketch, just select one face of the square. In the following image you can see, that I wanted to cut out an ellipse.

![Image19](../images/week02/3dParts/referenceBody02.jpg)

So after I have done my sketch, I just needed to extrude to the direction facing to the object. An indication that you are going to cut out something is that the "extruded" preview is red.

![Image20](../images/week02/3dParts/referenceBody03.jpg)

Here you can see the result of it:

![Image21](../images/week02/3dParts/referenceBody04.jpg)

I did the same to all the other figures but with different cutouts.

![Image22](../images/week02/3dParts/finishedDesign.jpg)

<br>

[Download 3D player token](../layoutfiles/PlayerToken_3d/PlayerToken3D_v4.f3d)

<br>