# 5. CNC Milling

This week I learned to work with a CNC milling machine.


## Fusion360 Preparations
To prepare a model in Fusion360, you need to switch to the "manufacturing" mode and create a new setup.

### General Setup
For the general setup like the start point and the area which is milled you need to create a new "setup".

 ![Image01](../images/week05/fusionSetup001.jpg)

 In the first tab you can setup the coordinates and at which position the milling proccess should start.
 ![Image02](../images/week05/fusionSetup002.jpg)
 ![Image03](../images/week05/fusionSetup003.jpg)

The "stock" tab is for the working area. your modell should fit in there otherewise the outcome could be corrupted and results in a unusable product.

 ![Image04](../images/week05/fusionSetup004.jpg)

 If your model is form-fit modelled, then you should consider to make an offset. Since the tool has its own dimensions, the offset ensures that Fusion360 calculates the corresponding paths  so everything fits (or let ou know if something is off).

 ![Image05](../images/week05/fusionSetup005.jpg)
 ![Image06](../images/week05/fusionSetup006.jpg)

<br><br>

### Adaptive Clearing

The short version of the explaination below is that Adaptive Clearing is ideal to remove alot of material and therefore perfectly to mill out the rough form of the model.

 ![Image07](../images/week05/fusionSetup007.jpg)

 Now it makes sense to setup your prefered milling heads since those dimensions are crucial for the path calculations.
 In my case I setup a 6mm flathead milling head with one tooth for the Adaptive Clearing. This is a quiet big milling head but I want to remove lots of material and therfore it fits the job.
 You can search for presets of different tools but I recommend to measure the dimensions of the mlling head in your possession and put in the dimensions manually.

 ![Image08](../images/week05/fusionSetup008.jpg)
 ![Image09](../images/week05/fusionSetup009.jpg)
 ![Image10](../images/week05/fusionSetup010.jpg)
 ![Image11](../images/week05/fusionSetup011.jpg)



After that we can start to setup the Adaptive Clearing proccess.

 ![Image12](../images/week05/fusionSetup012.jpg)
 ![Image13](../images/week05/fusionSetup013.jpg)
 ![Image14](../images/week05/fusionSetup015.jpg)

Now it is setup and you can try to simulate it. There you can see how long it is going to take, you can see the cutting paths and which areas are not going to be processed because of for example the limitations of your milling machine.

 ![Image15](../images/week05/fusionSetup016.jpg)

<br>

 ### Parallel Clearing

Again a short explaination for Parallel Clearing: Parallel Clearing removes les material since the steps are shorter in the cutting paths. Parallel Clearing fits especially for the job to mill round parts or details.

 ![Image16](../images/week05/fusionSetup014.jpg)
 ![Image17](../images/week05/fusionSetup017.jpg)
 ![Image18](../images/week05/fusionSetup018.jpg)


 ![Image19](../images/week05/fusionSetup019.jpg)
 ![Image20](../images/week05/fusionSetup020.jpg)



 ![Image21](../images/week05/fusionSetup021.jpg)
 ![Image22](../images/week05/fusionSetup022.jpg)


For my models I did not need more than one Adaptive Clearing process but if you want to setup a process for a specific part of your model you should add a new process with different parameters/tools.

If you are happy with the simulations you need to export every process as an ``` .prn ``` file.
Just right click on one processand fill out the machine type and where to save.
![Image23](../images/week05/fusionSetup023.jpg)
![Image24](../images/week05/fusionSetup024.jpg)


### Project related galery
In the images below you can see what I milled for my final project.

![Image23](../images/week05/fusion001.jpg)
![Image24](../images/week05/fusion002.jpg)
![Image25](../images/week05/fusion003.jpg)
![Image26](../images/week05/fusion004.jpg)
![Image27](../images/week05/fusion005.jpg)

<br>
<br>

## CNC Process and result Gallery
Here you can see some photos of the milling proccess, the milling tools used and the final result of my model. 
There are some important aspects for the x-, y- and z-homepoint this time. Due to the diameter of the milling head, you need to setup the x and y homepoint to the radius of the head. I also used a sensor to measure the z-homepoint so I needed to set the x- and y-homepoint first.

![Image28](../images/week05/cncProcess001.jpg)
![Image29](../images/week05/cncProcess002.jpg)
![Image30](../images/week05/cncProcess003.jpg)
![Image31](../images/week05/cncProcess004.jpg)
![Image32](../images/week05/cncProcess005.jpg)
![Image33](../images/week05/cncProcess006.jpg)
![Image34](../images/week05/cncProcess007.jpg)
![Image35](../images/week05/cncProcess008.jpg)
![Image36](../images/week05/cncProcess009.jpg)
![Image37](../images/week05/cncProcess010.jpg)
![Image38](../images/week05/cncProcess011.jpg)

<br>
<br>


## Download
- The Fusion360 file:
  - <a href="../layoutfiles/cncMilling/churchAndHouse_v5.f3d" download>churchAndHouse_v5.f3d</a>
- The ```.prn``` file for the Adaptive Clearing: 
  - <a href="../layoutfiles/cncMilling/churchAndHouseAdaptive01.prn" download>churchAndHouseAdaptive01.prn</a>
- The ```.prn``` file for the Parallel Clearing:
  - <a href="../layoutfiles/cncMilling/churchAndHouseParallel01.prn" download>churchAndHouseParallel01.prn</a>
