# 7. Electronics design

This week I worked on designing the electronics board for my final project.

## Input/Output parts
"D" stands for a digital pin
"A" stands for an analoge pin

- 9 x LED Stripes (9 x D)
- 4 x buttons (4 x D)
- 1x Joystick (1 x D, 2 x A)

## Electronics Parts

- 1x ATMEGA328p
- 2x 22pF C_1206
- 1x 100nF C_1206
- 1x 1uF C_1206
- 1x 10uF C_1206
- 1x 499R R_1206
- 1x 10kR R_1206
- 1x LED_1206 YELLOW
- 1x 16MHz CSM-7x-DU (quartz)
- 1x Pinheaders M02
- 2x Pinheaders M03
- 2x Pinheaders M07


## Useful links

- [Datasheet for the LED Strips](https://html.alldatasheet.com/html-pdf/553088/ETC2/WS2812/95/1/WS2812.html)
- [Tutorial for the Joystick](https://elekonika.com/analog-joystick-arduino-tutorial/)
- [Datasheet for the microcontroller ATMEGA328p](https://www.alldatasheet.com/datasheet-pdf/pdf/194805/ATMEL/ATMEGA48P.html)

## kiCad: setup libaries
Since I needed to design my own circuit, I needed to worj wit kiCad. For that I also needed three libraries named "fab", "eagle" and "crystal".
the fab library is needed for all the basic components for example resistors or capacitors. The eagle library is for special components like the type of specific pinheades. The crystal library is only for the quartz oscillator.

The fab library needs to be added via "Manage Symbol Libraries" and the eagle and crystal library via "Managa Footprint Libraries".

![Image01](../images/week07/kiCadLibrary01.jpg)

You need to write the file name of the library file by hand by the way.
For fab library:

![Image02](../images/week07/kiCadLibrary02.jpg)

For eagle and crystal library you also need to set the "Plugin Type" to "Eagle":

![Image03](../images/week07/kiCadLibrary03.jpg)

## kiCad: designing the circuit

For my final project I need an interface for a position selection. The board has 9x9 holes, therefore I need nine LED stripes with nine LEDs per stripe to light up every position. The joystick is going to be used to set the position. Also I need four buttons: one for the postion confirm and the other three for the maximum distance.

The LED stripes needs a digital pin each (= 9 D-pins), the joysticks requires two analoge pins (for x and y coordinates) and one digital pin for the button under the stick. (=> 2 A-pins, 1 D-pins) and the four buttons also needs a digital pin each (=> 4 D-pins). The total amount are 2 Analoge Pins and 14 Digital Pins.

Since the Attiny 44a mcirocontroller of week04 does not have enough digital pins, I decided to use the ATMEGA328P.
On the right hand side you can see all the pins and their corresponding connections. 


The first option (cirled in red, number 1) of the right hand side tool bar is the "place symbol" function, where you can search and select all the components of your component libraries like "fab".
The second option (cirled in blue, number 2) is used to draw the circuit connection lines.
The name tags are a "global lable" (cirled in green, number 3) and are used to reference a connection, to make the schematic more readable.

![Image05](../images/week07/kiCad02.jpg)

![Image06](../images/week07/kiCad03.jpg)

When you did all your connections, you need to click on "Assign PCB symbols to schematic symbols" (number 1 in red) to assign the compontents.
After you assigned the components, you can generate in the tool "Run Pcbnew to layout printed circuit board" (number 2 in blue)

![Image04](../images/week07/kiCad01.jpg)

On the left hand side you see the library frome where you selected the components. Select the corresponding inserted part and select on the right hand side the component from the library (red to red, blue to blue).
Click on apply and ok then process to "Run Pcbnew to layout printed circuit board".

![Image07](../images/week07/kiCad04.jpg)

In "Run Pcbnew to layout printed circuit board"  on number 1 (red) you import your previously made circuit. And now you just need to connect all the shown path with the toll at number 2 (blue). Alternatively if you need to connect multiple connections to for example GND, you can make an area, from where the traces can connect to the pin. For that use number 3 (green).

![Image08](../images/week07/kiCad05.jpg)


## Fabmodule and milling

After you have done the connections, you export it to a SVG file and follow all the steps from the reoprt of week04. 
In the following pictures you see the setting used in this week: 

Traces:

![Image14](../images/week07/traces_circuit-brd.png)
![Image09](../images/week07/fabmodule01.jpg)

Holes:

![Image12](../images/week07/holes_circuit-brd.png)
![Image10](../images/week07/fabmodule02.jpg)

Outline:
![Image13](../images/week07/outline_circuit-brd.png)
![Image11](../images/week07/fabmodule03.jpg)

The finished board:

![Image15](../images/week07/boardDone01.jpg)


## Gallery

Circuit milling:

<p class="pic"> <img src="https://gitlab.com/rofll38/patrick.klenner/-/raw/main/docs/images/week07/milling.gif"> </p>

The functioning board:

<p class="pic"> <img src="https://gitlab.com/rofll38/patrick.klenner/-/raw/main/docs/images/week07/blinking.gif"> </p>


## Download

 - kiCad Project as a zip: <a href="https://gitlab.com/rofll38/patrick.klenner/-/raw/main/docs/kiCad_Project/FoDF_W07.rar" download>FoDF_W07.rar</a>


