# 8. Embedded Programming

In this week I learned how to programm a microcontroller in the Arduino IDE.

## Basic Programming
Unlike programming for a computer or smartphone, a microcontroller does not have enough storage capacity to hold several Megabytes of Code or Data.
Therfore the code (including the used libraries) needs to fit on the flash memory. The ATmega328, which I use, has 32 KB of flash memory whihc is quiet a lot for a microcontroller. For comparison the ATtiny84 has only 8 KB of flash Memory.
A conventional computer today has atleast 8 GB RAM and a aditional internal HDD or SSD hard disk which also contains a storage space of several GB or even TB.

So the code needs to be pretty small and therefore very efficent. Since I do not need to programm so much code for my project, I use the buildin language of the Arduino IDE which is quiet similar to Java.
But notice: This language has already buildin functions and needs to be interpreted which also costs some memory space. If you want an optimised and pretty small code, you can try "C", "C++" or even "Assembly".

All the varaibles in the Arduino Language need to be defined by how much storage they use in Bytes.

Here are some data types in the Arduino Language:

 <table>
  <tr>
    <th>Data Types</th>
    <th>Size in Bytes</th>
    <th>Can contain</th>
  </tr>
  <tr>
    <td>boolean</td>
    <td>1</td>
    <td>true(1) or false(0)</td>
  </tr>
  <tr>
    <td>char</td>
    <td>1</td>
    <td>ASCII character or signed value between -128 and 127</td>
  </tr>
  <tr>
    <td>unsigned char, byte, uint8_t</td>
    <td>1</td>
    <td>ASCII character or unsigned value between 0 and 255</td>
  </tr>
  <tr>
    <td>int,short</td>
    <td>2</td>
    <td>signed value between -32,768 and 32,767</td>
  </tr>
  <tr>
    <td>unsigned int, word, uint16_t</td>
    <td>2</td>
    <td>unsigned value between 0 and 65,535</td>
  </tr>
  <tr>
    <td>long</td>
    <td>4</td>
    <td>signed value between -2,147,483,648 and 2,147,483,647</td>
  </tr>
  <tr>
    <td>unsigned long, uint32_t</td>
    <td>4</td>
    <td>unsigned value between 0 and 4,294,967,295</td>
  </tr>
  <tr>
    <td>float,double</td>
    <td>4</td>
    <td>floating point value between -3.4028235E+38 and 3.4028235E+38 (float is the same as double here)</td>
  </tr>
</table> 

And how to declare a varaible those types:

```
boolean running = false;


char myChar = 'A';
char myCHar = 65;

int ledPin = 13;

long speedOfLight = 186000L;

float sensorCalbrate = 1.117;

int myArray[10] = {9,3,2,4,3,2,7,8,9,11};
// the array index goes form 0 to 9, therefore 10 entries
```

All the syntax and sketch structure are very similar to Java, so you can do a Java tutorial to learn the syntax and structures. There are some buildin funcitons, which you can look up in the corresponding references of the libraries.



## Useful links

- [SoftwareSerial](https://www.arduino.cc/en/Reference/SoftwareSerial)
- [Arduino IDE](https://www.arduino.cc/en/guide/windows)
- [Java Tutorial](https://www.w3schools.com/java/)
- [Arduino Libraries References](https://www.arduino.cc/en/guide/libraries)


## Code Explaination

The following code let a LED blink on pin 5 in the pattern of the morse code of "SOS" two times and is written in the Arduino IDE.
To include a library you need to search for them in the libary manager:

![Image08](../images/week08/includeLibraries.jpg)

Unless it is a buildin library like "SoftwareSerial".

The ```#define``` sets a constant variable, which value can not be changed.
"SoftwareSerial" is a function of the Library "SoftwareSerial" and manages the serial communication between the microcontroller to the computer. It is useful, if you want to readout some analoge signals or other values.

The boolean "buttonState" should contain the value of the button state which is by default "HIGH" due to the pullup pins of the microcontroller (look it up in the datasheet).

Now there is a varaible for a counter to count the iterations.

The function "void setup()" is a prewritten function of the Arduino IDE where you define all the specification for example like the BAUD rate with "mySerial.begin(4800)"  that determine the speed of communication over a data channel (It is the unit for symbol rate or modulation rate in symbols per second or pulses per second). Also you define which pins are going to be set for input signal or output signals.

The "void loop()" function is also a prewrittten function and all the code in there is executed endless times.

In the comments (behind the ```//``` or between the  ```/* */``` )below you can read the important functionalities of my code.

```
#include <SoftwareSerial.h>

#define LED_PIN 5
#define BUTTON_PIN 6
#define ITERATIONS 2

SoftwareSerial mySerial(3,4); //RX;TX
boolean buttonState;
byte counter = 0;

void setup() {
  // put your setup code here, to run once:

  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);
  mySerial.begin(4800); // BAUD rate should not be more than 4800

}

void loop() {
  // put your main code here, to run repeatedly:
  buttonState = digitalRead(BUTTON_PIN);

  // the digital input pins of the ATmega328 are set to HIGH by default
  if( buttonState){
    // if the counter is lower than the presetted constant ITERATIONS, then execute the code inside the curly brackets
      if( counter < ITERATIONS)
      {
            morseSOS(); // calls the "morseSOS" function
            mySerial.print("Signal Iteration no. "); //prints the string (the character chain) into the serial monitor
            mySerial.println(counter + 1);
            counter++; // increases the varaible by 1
            delay(1000); // stop the code for 1 second
      }
    }
    else{
      counter = 0; // reset the counter
      serialCommunication(); // call the "serialCommunication" function
    }

  
  

  
}

// sends the message to the serial ouput
void serialCommunication(){
  mySerial.println("SOS signal restarted");
  delay(1000);
}

// set the LED to HIGH or LOW acording to the morse code for "SOS" (3 long, 3 short, 3 long)
void morseSOS(){
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);

  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(100);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(100);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(100);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(100);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(100);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);

  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);
  digitalWrite(LED_PIN, HIGH);  //this writes HIGH th the led pin
  delay(1000);
  digitalWrite(LED_PIN, LOW); //this writes LOW to the led pin
  delay(1000);



  
}

```

## Gallery
Here you can see the connections of my board:
![Image01](../images/week08/connection001.jpg)
![Image02](../images/week08/connection002.jpg)
![Image03](../images/week08/connection003.jpg)

With the corresponding Arduino pins to my board pins:
![Image04](../images/week08/arduinoPins01.jpg)
![Image05](../images/week08/boardPins01.jpg)
![Image06](../images/week08/boardPins02.jpg)

Here is the serial output (do not forget to switch the ports to the FTDI cable):
![Image07](../images/week08/serialOutput.jpg)

And last but not least the functional code in the board:

<p class="pic"> <img src="https://gitlab.com/rofll38/patrick.klenner/-/raw/main/docs/images/week08/sosMorseCode.gif"> </p>



## Download

- The morse code skript as a zip: <a href="https://gitlab.com/rofll38/patrick.klenner/-/raw/main/docs/skripts/FoDF_week08/week08_skript.rar" download>week08_skript.rar</a>
