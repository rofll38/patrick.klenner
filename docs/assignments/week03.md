# 3. Laser Cutting


This week I worked on designing the board and some game environment elements. These are the final designs for the lasercutter. 

## The Board


All the desgins were made with "Autodesk Fusion 360" and are only in 2D.
As a first sketch I draw the board with the first estimated dimensions.

 ![Image01](../images/week03/bord_plan_v02.jpg)

Since the dimensions of the lasercutter machine is 300mm x 600mm, I needed to adjust my sketch in Fusion 360 so I have some tolerances. As you can see the playground have the dimensions of 240mm x 240mm since the border of 50mm should ensure the stability of the whole board. With the "change parameters" function in Fusion360 you can preset some values like the size of the board and use it for any model. If you used them, the "fx: " indicates the usage.
![Image02](../images/week03/board01.jpg)

After that I placed the holes for the pins of the gmae elements, so I can use pins for the game piece positions.
The distance from the inner border and the outer pin holes is 17.5mm and the distance between the pinholes is 15mm. Also the size of these squares are 5mm x 5mm.
Since all the pinholes have the same size and distance to each other, I used the "rectangular pattern" tool in Fusion360 to duplicate the sketch for all the holes.

![Image03](../images/week03/board02.jpg)

For the chess-board-eque look I needed to draw the corresponding lines for the engraving. The measurements for the outer lines are 17.5mm. For the lines between the pinholes are 15mm long.
Like for the pinholes, I also used the "rectangular pattern" tool again but I did the outer pieces first then the inner ones.
In the following five pictures, you cann see the process of the sketch.
![Image04](../images/week03/board03.jpg)
![Image05](../images/week03/board04.jpg)
![Image06](../images/week03/board05.jpg)
![Image07](../images/week03/board06.jpg)
![Image08](../images/week03/board07.jpg)
![Image09](../images/week03/board08.jpg)
<br>
<br>


I wanted to make some buttons for every position and for those I needed some holes. The diameter of the circle is 10mm and I created te pattern again with the "rectangular pattern" function.
![Image10](../images/week03/board09.jpg)

And herer is the final design for the board:
![Image11](../images/week03/board10.jpg)

<br>
<a href="../layoutfiles/LaserCutting/BoardWood_v8.dxf" download></a>

<!-- 
[Download the board as .dxf file](../layoutfiles/LaserCutting/BoardWood_v8.dxf)
-->
<br>
<a href="../layoutfiles/LaserCutting/BoardWood_v8.f3d" download>Download the board as .f3d file</a>

<!-- [Download the board as .f3d file](../layoutfiles/LaserCutting/BoardWood_v8.f3d)
 -->
<br>
<br>
<br>

## Game Parts
The game parts are a wall and a door which can be placed on the board. These are the reason for the pinholes. The pin dimension is 5mm x 5mm.
You can also see all the measurements in the images below and all values are in mm.
![Image12](../images/week03/wall01.jpg)
![Image13](../images/week03/door01.jpg)

A test cutout of a wall is shown below:
![Image14](../images/week03/door02.jpg)

<br>
<a href="../layoutfiles/LaserCutting/BoardWood_v8.dxf" download>Download the door and wall as a .dxf file</a>

<!--
[Download the door and wall as a .dxf file](../layoutfiles/LaserCutting/BoardWood_v8.dxf)
-->


<br>
<a href="../layoutfiles/LaserCutting/BoardWood_v8.f3d" download>Download the door and wall as a .f3d file</a>
<!--
[Download the door and wall as a .f3d file](../layoutfiles/LaserCutting/BoardWood_v8.f3d)
-->

<br>


## Lasercutter settings
A best practise for cutting out the parts is to do the engraving first, then cut out the inner parts and finally the outer lines. I used the "Zing" lasercutter in the fablab in the university FabLab of "Hochschule-Rhein-Waal".

![Image15](../images/week03/zing.jpg)

In the following images you can see the settings for the different jobs:

First comes the engraving, so we want a high speed and low power. Usually the engraving is done with the "raster" methode but vetor is with the right settings fine too.
![Image16](../images/week03/lasercutSetting03.jpg)
![Image17](../images/week03/lasercutSetting02.jpg)

Now the inner cuttouts should be cut so the remaining material and the home point of the laser cutter is still fixed. This ensures that if you pause the process to check the cuts, you don't need to reconfigurate it again.
Since the material is to be cut, we need to ajust the settings to a high power and a low speed to penetrate through the material.
![Image18](../images/week03/lasercutSetting04.jpg)
![Image19](../images/week03/lasercutSetting05.jpg)

With the same settings we can cut out the outer parts:
![Image20](../images/week03/lasercutSetting06.jpg)


<br>
<br>
<br>

## Sneek Peek
The following pictures shows you the process of the cutting and the intermediate step of the final cut out of the board.

![Image21](../images/week03/sneekPeek01.jpg)
![Image22](../images/week03/sneekPeek02.jpg)
![Image23](../images/week03/sneekPeek03.jpg)



