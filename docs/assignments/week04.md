# 4. Electronics Production
In this week I learned how to cut out an circuit with a CNC machine and how to solder.


## Useful links

- [Fab Modules](http://fabmodules.org/)
- [Inkscape](https://inkscape.org/release/inkscape-1.1.1/)
- [GIMP](https://www.gimp.org/downloads/)
- [KiCAD](https://www.kicad.org/download/)

## Preparations
I got a test circuit layout to learn about the process of cutting out a circuit board with a CNC machine. 
At first I used "KiCAD" to export the prebuild circuit as a SVG file. After that I opened that svg file in "Inkscape" to edit the size and export it into a PNG file.

<br>

### Inkscape
 I used the settings marked in red to export the svg file into a png.
 ![Image01](../images/week04/prep01.jpg)

 Before that selected the circuit and added +3 **mm** to the width and height. Also I centered the circuit.

 ![Image02](../images/week04/prep02.jpg)

 <br>

### Gimp
The png file is now prepared and the different cutouts needs their own png file.
For that task I used Gimp, so I can edit the color areas corresponding to the cutout parts.

 ![Image03](../images/week04/prep03.jpg)
 ![Image04](../images/week04/prep04.jpg)
 ![Image05](../images/week04/prep05.jpg)
 ![Image06](../images/week04/prep06.jpg)
 ![Image07](../images/week04/prep07.jpg)
 ![Image09](../images/week04/prep09.jpg)
 ![Image10](../images/week04/prep10.jpg)
 ![Image11](../images/week04/prep11.jpg)
 ![Image12](../images/week04/prep12.jpg)

<br>

### FabModule
Now that the PNG files are prepared, the next step is to convert it into a file that the CNC machine can use. For that I uploaded the PNG files one by one with the following settings:

![Image13](../images/week04/fabModule01.jpg)
![Image14](../images/week04/fabModule02.jpg)
![Image15](../images/week04/fabModule03.jpg)

Also don't forget to click on "calculate" for the paths.

<br>

## CNC machine configuration

For the circuit I used a copper PCB plate to mill out the chip.
To configure the home position, I used "Roland VPanel" since the machine is a "Roland" mill. In the first Image below you can see how this software looks like:
 ![Image16](../images/week04/cncConfig01.jpg)

 There is a recommended order in which you should process your desired part. First should the traces be done because for those you only "scratch" the plate with .20mm milling head. After that you need to drill the most inner holes or lines and finally the cutout. To set the z-axis home position you need to first turn on the mill with the hihgest RPM (here: 15000) and let the milling head slowly on the board until it barely hits the surface. An indicator is that you will hear a slight cutting noise.
 ![Image17](../images/week04/cncConfig02.jpg)
 ![Image18](../images/week04/cncConfig03.jpg)
 ![Image19](../images/week04/cncConfig04.jpg)
 ![Image20](../images/week04/cncConfig05.jpg)
  
  
 After the traces comes the holes and finally the cutout. For that I used a thicker milling head since the head needs to drill through and around the chip. For that I used the milling head with the 0.6 diameter. THe only configuration to change is the z-axsis homeposition since we want to go through the board.

 ![Image21](../images/week04/cncConfig06.jpg)
 

<br>

## Soldering

Now the chip is cutout and now it is time to solder all the components for the chip. The number after the "M" of the pin headers tells you how many pins you should have together.

The parts are:

- 1x "R1206 10k ohms resistor"
- 1x "R1206 499 ohms resistor"
- 1x "LED1206 Yellow"
- 1x "C1206 100nF Capacitor"
- 1x "C1206 1uF Capacitor"
- 1x "C1206 10uF Capacitor"
- 1x "pinheaders M01" 
- 2x "pinheaders M02"
- 1x "pinheaders M05"  
- 1x "ATTiny45 8Mhz" Microcontroller

I recommend to solder the Microcontroller first and then the rest. In the next image you can see an "easy" way to solder the pins of the Microcontroller.
 ![Image22](../images/week04/soldering01.jpg)

 After all the hard work this beauty was born:
 ![Image23](../images/week04/soldering02.jpg)
 ![Image24](../images/week04/soldering03.jpg)

 Now it is time to test if the little guy also lives:
 ![Image25](../images/week04/soldering04.jpg)
 ![Image26](../images/week04/soldering05.jpg)
 ![Image27](../images/week04/soldering06.jpg)

 I put the instructions of how to program this specific mcirocontroller with the Arduino IDE in the downloadable files. (Follow the instructions CAREFULY, also "Upload using programmer" isn't the usual uploading button by the way)

Look how happy it is:

<img src="../images/week04/blinkDemo.gif">




<br>

## Downloads
Pngs files:

- <a href="../kiCad_Project/hello_board/circuitLayout/cutout.png" download>cutout.png</a>
- <a href="../kiCad_Project/hello_board/circuitLayout/holes.png" download>holes.png</a>
- <a href="../kiCad_Project/hello_board/circuitLayout/traces.png" download>traces.png</a>

And the .rml files:

- <a href="../kiCad_Project/hello_board/circuitLayout/cutout.rml" download>cutout.rml</a>
- <a href="../kiCad_Project/hello_board/circuitLayout/holes.rml" download>holes.rml</a>
- <a href="../kiCad_Project/hello_board/circuitLayout/traces.rml" download>traces.rml</a>

The programming instructions:

- <a href="../kiCad_Project/hello_board/Programming/how-to-program.txt" download>how-to-program.txt</a>

