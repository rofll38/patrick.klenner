# 10. Output Devices

This week I learned about several output devices and worked with a LED stripe.

## General Specifiactions
The LEDs are powered with 5V but since I only need 81 of them, I needed to test the ampere by turning 81 LEDs to the maximum brightness, so I can measure the maximum apmere for powering all the used LEDs.
As you can see in the image below the LEDs consumes 0.63A.
The LEDs have three connections: one for Vcc one for GRND and the middle one for DATA.
You control with the DATA connection all the settings like brigthness, color and whcih LED should be turned on.

![Image03](../images/week10/voltsAndAmpere.jpg)

I also measured that I need a 2A powersource for a stable power supply.
My plan is to use a powerbank as a power source therefore I need a power supply module for the USB connection. 



## Useful links

- [FastLED Library](http://fastled.io/)
- [Power Supply Datasheet](https://components101.com/sites/default/files/component_datasheet/MB102-Datasheet.pdf)
- [LED stripe Datasheet](https://html.alldatasheet.com/html-pdf/553088/ETC2/WS2812/95/1/WS2812.html)
- [LED stripe tutorial](https://www.electroniclinic.com/arduino-ws2812b-led-strip-connection-and-code/)

## Code Explaination

The code tests my LEDs and let them blink. To use the LED stripe it is recommended to use the "FastLED" library of the Arduino IDE.

```
#include <FastLED.h>
#define LED_PIN A2
#define NUM_LEDS 81

CRGB leds[NUM_LEDS];  // set an array for the used LEDs
void setup() {
  // put your setup code here, to run once:
  FastLED.addLeds<WS2812, LED_PIN, RGB>(leds, NUM_LEDS);  // set the manufacturing type of LED stripes, the LED pin and the type of the LED
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 2000);        // set the maximum Volts and Ampere

  FastLED.clear();    //clear out the data
 
  FastLED.show();     //send the data to the LEDs
}

void loop() {
  // put your main code here, to run repeatedly:
  // RED Green Blue

  //the LEDs needs to be activated first
  for (int i=0; i<NUM_LEDS; i++ )
  {
      leds[i] = CRGB(255, 255, 255 );   //configures the color of the LEDs
      FastLED.setBrightness(255);       //configures the brightness
      FastLED.show();

  } 

  // let all 81 LEDs blink
  for (int i=0; i<NUM_LEDS; i++ )
  {
      leds[i] = CRGB(255, 255, 255 );
      FastLED.setBrightness(255);
      FastLED.show();
      delay (1000);
      FastLED.setBrightness(0);
      FastLED.show();
      delay (1000);
  } 

}
```


## Gallery


Here you can see the connections for the LED matrix:
![Image01](../images/week10/boardConections01.jpg)
![Image02](../images/week10/boardConections02.jpg)
![Image04](../images/week10/WithPowerSupply01.jpg)
![Image05](../images/week10/WithPowerSupply02.jpg)


The working code:

<p class="pic"> <img src="https://gitlab.com/rofll38/patrick.klenner/-/raw/main/docs/images/week10/LedMatrixBlink.gif"> </p>


## Download

- The LED matrix blink code skript as a zip: <a href="https://gitlab.com/rofll38/patrick.klenner/-/raw/main/docs/skripts/FoDF_week10/FoDF_week10.rar" download>FoDF_week10.rar</a>
