# 1. Project Management

This week I worked on configurating my workspace by learning how git lab works and how to work with Markdown.

## Useful links

- [Markdown](https://en.wikipedia.org/wiki/Markdown)
- [Code Blocks in Markdown](https://www.tutorialsandyou.com/markdown/how-to-write-code-blocks-in-markdown-8.html)
- [MarkdownTutorial](https://www.markdowntutorial.com/)


## Why using Git Lab?
Everyone who works with computers knows the struggle: You have done an amazing presentation, code, concept art, etc. but after you have finished your work you spilled your coffee on your computer. Everything is lost and you need to start form scratch. Another scenario could be that a coworker just had overwritten some important files and you have no chance to recover all the data. To prevent that one might argue that you should have a RAID system operating. But a RAID system is costly and a bit of an overkill if you just have small changes.
With Git Lab everyone have an opportunity to have a recovery system for their private or job related projects. I  gerneral Git Lab only saves only the changes which were done on the project and not the entire data of a project. This is a much faster approach since the data which is needed to be processed is much smaller and therefore much faster to save.
Git Lab is also based on a change history concept, which documents all the changes and creates "checkpoints" at the same time the changes are applied.

### Command Example
To work with Git Lab one need to know how to set up the local machine for Git and Git Lab and most importantly how to connect the local Git with a Project on Git Lab. If you have already Git installed on your local device, the following commands as shown as  the following Image, will configure Git for further useage. 

In line 80 you can see that you need to set your username and email adress to clarify  in the change history of the project on Git Lab, who had done a change at the corresponding time. 
The lines 83 to 85 are responsable for a secure connection between the local device and the Git Lab Server. The generated public key needs to be inserted by hand in the project settings of Git Lab. 
The line 95 clones (makes a copy of the project) of the project. In this case the project "patrick.klenner.git" is cloned.

Now the local device is setup to use the Git Lab functionalities and as a test the file "test.txt" is added with ```git add <filename.filetype>``` to the changes in line 103. With ```git status``` one can check, which changes of files, directories, etc. are not added and commited yet. So after the git add one needs to tell the Git that the changes are ready to send. With the command ```git commit -m "<commit message>"``` does the job. Also be aware the this command NEEDS the ```-m "<commit message>"``` part otherwise one just get problems. So always use the commit message.
If one has made more changes than just a file like in my case, with the command 
```git add --all``` one can add all the changes to the commit list.
After the commit one needs to send those changes to the Git Lab Project. Like in line 114 with ```git push``` all the commited changes are send to the Git Lab project. The ```git pull`` command is to get all the changes which were done, so basically to get the newes versions of the project files.
##

<!-- images in Markdown -->
![Image1](../images/week01/GitLabCommands.jpg)
<br>
<br>
<br>
<br>
<br>
<br>
## Markdown

Markdown is a lightweight markup language for creating formatted texts in a plain text-editor. It's an alternative for HTML espacially for those who does not work with HTML so often. Since Markdown is based on HTML, one can use HTML commands like ```<div></div>```.
To write in italic one needs to put the words between two "_":  ```_italic_``` .
For bold text the text needs to be written like in the following example: ```**blod**```.

If some headers are needed, just write a "#" for every size. One "#" is the biggest and if one uses more like ```##``` and ```###``` and so on, the smaller the headline gets.
Just Like in this page which you are currently reading:
![Image2](../images/week01/markdownExample.jpg)

Like in the Image above (below "## Useful Links"), one can create links by using the following syntax: ```[Text For Your Link](the url of the website of your choise) ```. To insert an image in a markdown file is quite commn to the link syntax, jsut add a "!" before the squared brakets: ```[Name For Your Link](the url of the website of your choise)```.

To create lists only one ```*``` is needed for each list item:
* milk
* water
* beer
* burgers

The semantic for the shown list example: 

![Image3](../images/week01/markdownListExample.jpg)

Alternatively one can use numbers instead of the "*": ```1. item```


